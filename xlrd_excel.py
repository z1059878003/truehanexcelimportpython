import json
from jsonModel import jsonModel


@jsonModel()
class Pet(object):

    def __init__(self):
        self.cardName = ""
        self.cardOpeningDate = ""
        self.cardExpireDate = ""
        self.price = ""
        self.totalTimes = 0
        self.memberTimes = 0
        self.paymentMethod = ""
        self.numberOfPauses = 0


@jsonModel()
class Prepaid(object):

    def __init__(self):
        self.prepaidCardName = ""
        self.cardOpeningDate = ""
        self.cardExpireDate = ""
        self.price = ""
        self.giftAmount = ""
        self.paymentMethod = ""


@jsonModel({"pets": Pet}, {"prepaid": Prepaid})
class Person(object):
    def __init__(self):
        self.clubId = 5
        self.gender = ""
        self.birthday = ""
        self.phoneNumber = ""
        self.name = ""
        self.idCardNo = ""
        self.school = ""
        self.nickname = ""
        self.contractNo = ""
        self.excelCardNameList = []
        self.excelPrepaidCardList = []
