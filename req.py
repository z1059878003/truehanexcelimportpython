# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pandas as pds
import requests
import json
import pandas as pd

def request():
    eal_data = {
        "clubId": 6,
        "gender": "男",
        "birthday": "2000-10-12",
        "phoneNumber": "18670361589",
        "name": "沈莲2",
        "idCardNo": "321002201403014912",
        "school": "耀华小学",
        "nickname": "沈亚莲",
        "contractNo": "30020",
        "excelCardNameList": [{
            "cardName": "瑜伽时间卡",
            "cardOpeningDate": "2022-02-20",
            "cardExpireDate": "2023-02-19",
            "price": "5980",
            "totalTimes": 53,
            "memberTimes": 40,
            "paymentMethod": "支付宝",
            "numberOfPauses": 0
        }, {
            "cardName": "测试卡",
            "cardOpeningDate": "2022-02-20",
            "cardExpireDate": "2023-02-19",
            "price": "6000",
            "totalTimes": 60,
            "memberTimes": 40,
            "paymentMethod": "支付宝",
            "numberOfPauses": 0
        }]
    }

    head = {"Content-Type": "application/json; charset=UTF-8", 'Connection': 'close'}

    url_d = "http://127.0.0.1:8301/miniapp/members/anon/excel/add/member"

    json_d = json.dumps(eal_data)

    res_d = requests.post(url=url_d, data=json_d, headers=head)

    df = pd.read_excel("小程序会员测试数据.xlsx", sheet_name="会员卡", parse_dates=[2, 3],
                       date_parser=lambda x: pd.to_datetime(x, format='%Y/%m/%d'))

    df2 = pd.read_excel("小程序会员测试数据.xlsx", sheet_name=[0, 1])
    print("\n(2)第2行第3列的值：")
    print(df.values[1, 2])
    print("\n(3)第1行数据：")
    print(df.values[0])
    print("\n(4)获取第2、3行数据：")
    print(df.values[[1, 2]])
    print("\n(5)第2列的数据：")
    print(df.values[:, 1])
    print("\n(6)第2,3列数据:")
    print(df.values[:, [1, 2]])
    print("\n(7)第1至3行,4到6列数据：")
    print(df.values[0:3, 3:6])
if __name__ == '__main__':
    request()
    str(1)
    print(str(12))
