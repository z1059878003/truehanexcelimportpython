# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import json

import numpy as np
import pandas as pds
import requests

from pandas import DataFrame

from xlrd_excel import Person, Pet, Prepaid


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    # print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    pass


def red_execl():
    user_infos = pds.read_excel("小程序会员测试数据.xlsx", sheet_name="用户信息")
    # df_dict = {}

    for info in user_infos.index.values:
        df_line = user_infos.loc[info]
        member_cards = pds.read_excel("小程序会员测试数据.xlsx", sheet_name="会员卡", index_col="会员编号")
        id = df_line['会员编号']
        # 会员卡
        # 基本信息
        person = Person()
        person.name = df_line['学员姓名（真实姓名）']
        person.gender = df_line['性别']
        person.birthday = df_line['生日']
        person.idCardNo = str(df_line['身份证号码'])
        person.school = df_line['在读学校']
        person.nickname = df_line['联系人（会员名）']
        person.phoneNumber = str(df_line['联系方式（手机号）'])
        person.contractNo = str(df_line['合同编号'])
        member_prepaid_card = pds.read_excel("小程序会员测试数据.xlsx", sheet_name="储值卡", index_col="会员编号")
        # 会员卡
        df_list = []
        # 储值卡
        pc_list = []
        pet = Pet()
        member_card = member_cards.loc[id]

        # print(member_card)
        if isinstance(member_card, DataFrame):
            for aa in member_card.iloc:
                pet.cardName = aa['卡片名称']
                pet.cardOpeningDate = str(aa['开卡日期'])
                pet.cardExpireDate = str(aa['卡片有效期'])
                pet.price = str(aa['报名金额'])
                pet.totalTimes = int((np.int64(aa['报名课次'])))
                pet.memberTimes = int((np.int64(aa['剩余课次'])))
                pet.paymentMethod = aa['支付方式']
                pet.numberOfPauses = int((np.int64(aa['可暂停次数'])))
                df_list.append(pet.toKeyValue())
            # print(df_list)-
            person.excelCardNameList = df_list

        else:
            pet.cardName = member_card['卡片名称']
            pet.cardOpeningDate = str(member_card['开卡日期'])
            pet.cardExpireDate = str(member_card['卡片有效期'])
            pet.price = str(member_card['报名金额'])
            pet.totalTimes = int((np.int64(member_card['报名课次'])))
            pet.memberTimes = int((np.int64(member_card['剩余课次'])))
            pet.paymentMethod = member_card['支付方式']
            pet.numberOfPauses = int((np.int64(member_card['可暂停次数'])))
            df_list.append(pet.toKeyValue())
            person.excelCardNameList = df_list
        try:
            member_prepaid_card = member_prepaid_card.loc[id]
            prepaid = Prepaid()
            if isinstance(member_prepaid_card, DataFrame):
                for aa in member_prepaid_card.iloc:
                    prepaid.prepaidCardName = aa['卡片名称']
                    prepaid.cardOpeningDate = str(aa['开卡日期'])
                    prepaid.cardExpireDate = str(aa['卡片有效期'])
                    prepaid.price = str(aa['充值金额'])
                    prepaid.giftAmount = str(aa['赠送金额'])
                    prepaid.paymentMethod = aa['支付方式']
                    pc_list.append(prepaid.toKeyValue())
                # print(df_list)
                person.excelPrepaidCardList = pc_list
            else:
                prepaid.prepaidCardName = member_prepaid_card['卡片名称']
                prepaid.cardOpeningDate = str(member_prepaid_card['开卡日期'])
                prepaid.cardExpireDate = str(member_prepaid_card['卡片有效期'])
                prepaid.price = str(member_prepaid_card['充值金额'])
                prepaid.giftAmount = str(member_prepaid_card['赠送金额'])
                prepaid.paymentMethod = member_prepaid_card['支付方式']
                pc_list.append(prepaid.toKeyValue())
                person.excelPrepaidCardList = pc_list
        except:
            person.excelPrepaidCardList = pc_list

        # print(json.dumps(person.toKeyValue()))

        red_json = json.dumps(person.toKeyValue())
        print(red_json)

        head = {"Content-Type": "application/json; charset=UTF-8", 'Connection': 'close',
                "Accept": "application/json, text/javascript, */*; q=0.01",
                "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
                "Accept-Encoding": "gzip, deflate, br",
                "X-Requested-With": "XMLHttpRequest", }

        url_d = "https://cloudapi.truehan.cn/miniapp/members/anon/excel/add/member"

        res_d = requests.post(url=url_d, data=red_json, headers=head, verify=False)

        print(res_d)


if __name__ == '__main__':
    red_execl()
# 391
